from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
User = get_user_model()


class SignUpSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=128, required=True, validators=[validate_password])
    confirm_password = serializers.CharField(max_length=128, required=True)

    class Meta:
        model = User
        fields = ('email', 'full_name', 'province', 'profession', 'password', 'confirm_password',)
        extra_kwargs = {
            'password': {'write_only': True},
            'confirm_password': {'write_only': True}
        }

    def validate(self, attrs):
        """
        The function validates that the password and confirm_password fields in the attrs dictionary
        match.

        :param attrs: The `attrs` parameter is a dictionary that contains the attributes or fields that
        need to be validated. In this case, it is expected to contain the fields `password` and
        `confirm_password`
        :return: The `attrs` dictionary is being returned.
        """
        if attrs['password'] != attrs['confirm_password']:
            raise ValidationError({
                'message': "The two password fields didn't match"
            })

        return attrs


class LoginSerializer(serializers.Serializer):
    username = serializers.EmailField(required=True)
    password = serializers.CharField(required=True, write_only=True)

    class Meta:
        fields = ('username', 'password')

    def validate(self, attrs):
        """
        It checks if the user exists, if the user is active and verified, and if the password is correct

        :param attrs: The validated data from the serializer
        :return: The user object is being returned.
        """
        authenticate_kwargs = {
            'username': attrs.get('username'),
            'password': attrs.get('password')
        }
        try:
            authenticate_kwargs['request'] = self.context['request']
        except KeyError:
            pass

        if not User.objects.filter(email=attrs.get('username')).exists():
            raise ValidationError(
                {'message': 'No account found with this email.'
                 })

        user = User.objects.filter(email=attrs.get('username')).first()

        user = authenticate(**authenticate_kwargs)
        if user is None:
            raise ValidationError({
                'message': 'Incorrect email or password.'
            })
        attrs['user'] = user
        return attrs


class RefreshTokenSerializer(serializers.Serializer):
    refresh_token = serializers.CharField()
