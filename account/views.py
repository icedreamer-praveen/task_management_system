import logging

from django.contrib.auth import get_user_model
from drf_spectacular.utils import extend_schema
from rest_framework import generics, permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken

from .serializers import SignUpSerializer, LoginSerializer, RefreshTokenSerializer

User = get_user_model()


logger = logging.getLogger(__name__)


@extend_schema(summary="User Signup", tags=["Account"])
class SignUpAPIView(generics.CreateAPIView):
    serializer_class = SignUpSerializer
    queryset = User.objects.all()
    permission_classes = (permissions.AllowAny,)

    def create(self, request, *args, **kwargs):
        """
        POST method to create a new user.

        Request Data:
        - Data fields required to create a new user.

        Response Format:
        - If the user is successfully registered:
            {
                'message': "User registered successfully",
                'access_token': <access_token>,
                'refresh_token': <refresh_token>
            }
            Status Code: 201 CREATED

        - If the request data is invalid:
            {
                <Serializer errors>
            }
            Status Code: 400 BAD REQUEST
        """
        try:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            validated_data = serializer.validated_data
            validated_data.pop('confirm_password')
            password = validated_data.pop('password')

            user = User(**validated_data)
            user.set_password(password)
            user.is_verified = True
            user.save()

            refresh = RefreshToken.for_user(user)
            access_token = str(refresh.access_token)
            refresh_token = str(refresh)

            response_data = {
                'message': "User registered successfully",
                'access_token': access_token,
                'refresh_token': refresh_token,
            }
            return Response(response_data, status=status.HTTP_201_CREATED)

        except Exception as e:
            logger.error(f"Error creating user: {str(e)}")
            return Response({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@extend_schema(summary="User Login", tags=["Account"])
class LoginAPIView(APIView):
    serializer_class = LoginSerializer
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        """
        POST method to authenticate and generate access tokens for a user.

        Request Data:
        - Data fields required for user authentication.

        Response Format:
        - If the login is successful:
            {
                'message': 'Login Successful',
                'full_name': <User full name>,
                'user_id': <User ID>,
                'access_token': <Access token>,
                'refresh_token': <Refresh token>
            }
            Status Code: 200 OK

        - If the request data is invalid:
            {
                <Serializer errors>
            }
            Status Code: 400 BAD REQUEST
        """
        try:
            serializer = self.serializer_class(
                data=request.data,
                context={'request': request}
            )
            serializer.is_valid(raise_exception=True)
            user = serializer.validated_data['user']

            refresh = RefreshToken.for_user(user)
            access_token = str(refresh.access_token)
            refresh_token = str(refresh)

            logger.info(f"User '{user.full_name}' logged in successfully.")
            return Response({
                'message': 'Login Successful',
                'full_name': user.full_name,
                'user_id': user.id,
                'access_token': access_token,
                'refresh_token': refresh_token,
            }, status=status.HTTP_200_OK)

        except Exception as e:
            logger.error(f"Login failed: {str(e)}", exc_info=True)
            return Response(
                {"message": "Login failed", "errors": str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )


class LogoutAPIView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = RefreshTokenSerializer

    @extend_schema(summary="User Logout", tags=["Account"])
    def post(self, request):
        """
        The above function is a logout endpoint...
        """
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            refresh_token = serializer.validated_data['refresh_token']

            try:
                token = RefreshToken(token=refresh_token)
                token.blacklist()
                return Response({
                    'message': 'Logout successful'
                }, status=status.HTTP_200_OK)
            except Exception as e:
                return Response({
                    'message': f'Error during logout: {str(e)}'
                }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
