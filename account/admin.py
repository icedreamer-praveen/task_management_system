from django.contrib import admin
from django.contrib.auth import get_user_model

User = get_user_model()


@admin.register(User)
class CustomUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'full_name', 'email')
    list_display_links = (
        'full_name',
    )

    def get_readonly_fields(self, request, obj=None):
        """
        This function returns a tuple of read-only fields for a Django model object, based on whether the
        object exists or not.

        :param request: The HTTP request object that is sent to the server when a user interacts with the
        website
        :param obj: The obj parameter refers to the object being edited or viewed. If obj is None, it
        means that a new object is being created. If obj is not None, it means that an existing object is
        being edited
        :return: If an object `obj` is provided, the method returns a tuple of fields that should be
        read-only for that object. These fields are `'last_login'`, `'created_at'`, `'updated_at'`, and
        `'verification_link_expiration'`. If `obj` is not provided, an empty list is returned.
        """
        if obj:
            return (
                'last_login', 'created_at')
        else:
            return []

    @admin.display(description='Staff', boolean=True)
    def staff_status(self, obj):
        """
        The function checks if an object has staff status.

        :param self: The first parameter, "sef", is likely a typo and should be "self". It is a reference
        to the instance of the class that the method is being called on
        :param obj: The "obj" parameter is an object that is being passed into the "staff_status"
        function. The function is checking whether this object has a "is_staff" attribute and returning
        its value
        :return: a boolean value indicating whether the given object has staff status or not.
        """
        return obj.is_staff

    @admin.display(description='SuperUser', boolean=True)
    def superuser_status(self, obj):
        """
        This function returns a boolean value indicating whether the given object is a superuser or not.

        :param obj: The object being passed as an argument to the function. It is likely an instance of a
        user model or a related model that has a boolean field indicating whether the user is a superuser
        or not. The function returns the value of this boolean field for the given object
        :return: a boolean value indicating whether the given object has superuser status or not.
        """
        return obj.is_superuser
