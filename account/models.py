
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.core.mail import send_mail
from django.db import models
from django.utils.translation import gettext_lazy as _

from .managers import UserManager


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class User(AbstractBaseUser, PermissionsMixin, TimeStamp):
    full_name = models.CharField('User Full Name', max_length=255)
    province = models.CharField('Province', max_length=100)
    email = models.EmailField('Email Address', unique=True)
    profession = models.CharField("Profession", max_length=255)
    is_verified = models.BooleanField('Verified', default=False)
    is_active = models.BooleanField('Active', default=True,
                                    help_text=_(
                                        "Designates whether this user should be treated as active. "
                                        "Unselect this instead of deleting accounts."
                                    ), )
    is_staff = models.BooleanField('Staff Status', default=False,
                                   help_text=_("Designates whether the user can log into this admin site."),
                                   )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name']

    objects = UserManager()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
        ordering = ('-id',)
        indexes = [
            models.Index(fields=('email',)),
        ]

    def send_email(self, subject, html_message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, html_message, from_email, [self.email], html_message=html_message, **kwargs)

    def __str__(self):
        return f"{self.full_name}"
