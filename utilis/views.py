from django.shortcuts import render


def signup(request):
    return render(request, 'signup.html')


def login(request):
    return render(request, 'login.html')


def tasks(request):
    return render(request, 'tasks.html')


def task_create(request):
    return render(request, 'create_task.html')


def task_update(request):
    return render(request, 'update_task.html')


def task_delete(request):
    return render(request, 'delete_task.html')
