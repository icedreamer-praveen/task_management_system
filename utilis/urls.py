from django.urls import path
from .views import *

app_name = "utilis"

urlpatterns =[
    path("signup/", signup, name="signup-user"),
    path("login/", login, name="login-user"),
    path("tasks/", tasks, name="task-list"),
    path("task-create/", task_create, name="task-create"),
    path("task-update/", task_update, name="task-update"),
    path("task-delete/", task_delete, name="task-delete"),
    
]