from django.urls import path

from .views import TaskCreateAPIView, TaskListAPIView, TaskUpdateAPIView, TaskDeleteAPIView, TaskDetailAPIView

app_name = "task"

urlpatterns = [
    path('task-create/', TaskCreateAPIView.as_view(), name='task-create-api'),
    path('tasks/', TaskListAPIView.as_view(), name='task-list-api'),
    path('task-detail/<int:pk>/', TaskDetailAPIView.as_view(), name='task-detail-api'),
    path('task-update/<int:pk>/', TaskUpdateAPIView.as_view(), name='task-update-api'),
    path('task-delete/<int:pk>/', TaskDeleteAPIView.as_view(), name='task-delete-api'),
]
