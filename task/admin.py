from django.contrib import admin
from .models import Task


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "is_completed")
    list_display_links = (
        "title",
    )
    list_filter = ("is_completed", )
    search_fields = ("name", )
