import logging

from django.core.exceptions import ObjectDoesNotExist
from drf_spectacular.utils import extend_schema
from rest_framework import status, permissions
from rest_framework.exceptions import NotFound, APIException
from rest_framework.response import Response
from rest_framework.views import APIView

from logger_helper_function.interceptor import log_methods
from .models import Task
from .serializers import TaskCreateSerializer, TaskListAndUpdateSerializer

logger = logging.getLogger(__name__)


@log_methods
class TaskCreateAPIView(APIView):
    """
    API view to create a new task.
    Requires the user to be authenticated.
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TaskCreateSerializer

    @extend_schema(summary="Task Create API", tags=['Task'])
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            self.perform_create(serializer)
            return Response(
                {
                    "message": "Task Created Successfully",
                    "data": serializer.data
                },
                status=status.HTTP_201_CREATED
            )
        return Response(
            {
                "message": "Task Creation Unsuccessful",
                "errors": serializer.errors
            },
            status=status.HTTP_400_BAD_REQUEST
        )

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


@log_methods
class TaskListAPIView(APIView):
    """
    API view to list all tasks for the authenticated user.
    Requires the user to be authenticated.
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TaskListAndUpdateSerializer

    @extend_schema(summary="Task List API", tags=['Task'])
    def get(self, request, *args, **kwargs):
        try:
            logger.info("Fetching tasks for user")
            tasks = Task.objects.filter(user=request.user)
            serializer = self.serializer_class(tasks, many=True)
            return Response(
                {
                    "message": "Tasks fetched successfully.",
                    "data": serializer.data
                },
                status=status.HTTP_200_OK
            )
        except ObjectDoesNotExist:
            raise NotFound("No tasks found.")
        except Exception as e:
            logger.error(f"Error in get method: {str(e)}")
            return Response(
                {"message": str(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )


@log_methods
class TaskDetailAPIView(APIView):
    """
    API view to retrieve details of a specific task for the authenticated user.
    Requires the user to be authenticated.
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TaskListAndUpdateSerializer

    def get_object(self, pk):
        try:
            logger.info(f"Fetching task with pk={pk} for user={self.request.user}")
            return Task.objects.get(pk=pk, user=self.request.user)
        except Task.DoesNotExist:
            raise NotFound("Task not found.")
        except Exception as e:
            raise APIException(str(e))

    @extend_schema(summary="Task Detail API", tags=['Task'])
    def get(self, request, pk, *args, **kwargs):
        try:
            task = self.get_object(pk)
            serializer = self.serializer_class(task)
            return Response(
                {
                    "message": "Task detail fetched successfully.",
                    "data": serializer.data
                },
                status=status.HTTP_200_OK
            )
        except NotFound as e:
            logger.error(f"Task detail not found: {str(e)}")
            return Response({"message": str(e)}, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            logger.error(f"Error fetching task detail: {str(e)}")
            return Response({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@log_methods
class TaskUpdateAPIView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TaskListAndUpdateSerializer

    @staticmethod
    def get_object(user, pk):
        try:
            logger.info(f"Fetching task with pk={pk} for user={user}")
            return Task.objects.get(user=user, pk=pk)
        except Task.DoesNotExist:
            raise NotFound("Task not found.")
        except Exception as e:
            raise APIException(str(e))

    @extend_schema(summary="Task Update API", tags=['Task'])
    def put(self, request, pk, *args, **kwargs):
        try:
            task = self.get_object(request.user, pk)
            serializer = self.serializer_class(task, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": "Task updated successfully",
                    "data": serializer.data
                }, status=status.HTTP_200_OK)
            return Response({
                "message": "Task update unsuccessful",
                "data": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error(f"Error updating task with pk={pk}: {str(e)}")
            return Response({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @extend_schema(summary="Task Update API", tags=['Task'])
    def patch(self, request, pk, *args, **kwargs):
        try:
            task = self.get_object(request.user, pk)
            serializer = self.serializer_class(task, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": "Task updated successfully",
                    "data": serializer.data
                }, status=status.HTTP_200_OK)
            return Response({
                "message": "Task update unsuccessful",
                "data": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error(f"Error updating task with pk={pk}: {str(e)}")
            return Response({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@log_methods
class TaskDeleteAPIView(APIView):
    """
    API view to delete a specific task for the authenticated user.
    Requires the user to be authenticated.
    """
    permission_classes = (permissions.IsAuthenticated,)

    @staticmethod
    def get_object(user, pk):
        try:
            return Task.objects.get(user=user, pk=pk)
        except Task.DoesNotExist:
            raise NotFound("Task not found.")

    @extend_schema(summary="Task Delete API", tags=['Task'])
    def delete(self, request, pk, *args, **kwargs):
        """
        Delete a specific task identified by its primary key (`pk`).
        """
        try:
            task = self.get_object(request.user, pk)
            task_id = task.id
            logger.info(f"Deleting task with ID {task_id} for user {request.user.full_name}")

            task.delete()

            logger.info(f"Task with ID {task_id} deleted successfully.")
            return Response({
                "message": "Task deleted successfully."
            }, status=status.HTTP_204_NO_CONTENT)
        except NotFound as e:
            logger.error(f"Task not found with ID {pk} for user {request.user.full_name}")
            return Response({"message": str(e)}, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            logger.error(f"Failed to delete task with ID {pk} for user {request.user.full_name}: {str(e)}")
            return Response({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



