from django.db import models
from django.contrib.auth import get_user_model
from account.models import TimeStamp

User = get_user_model()


class Task(TimeStamp):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user")
    title = models.CharField(max_length=255)
    description = models.TextField()
    is_completed = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Task"
        verbose_name_plural = "Tasks"

    def __str__(self):
        return f"{self.title}"
