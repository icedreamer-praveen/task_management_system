## Guidelines to run project

1. After successful clone of the project please create the application-dev.yml file from the `.application-dev.yml.sample` file
2. create the database in postgres and update the value in application-dev.yml file accordingly
3. Also update the `POSTGRES_DB`, `POSTGRES_USER` and `POSTGRES_PASSWORD` too in docker-compose.yml file, since i haven't use from .env file.
4. After successfully setup the project run following command accordingly.
`- Example: sudo docker compose build and sudo docker compose up`
5. Get into the container.
`- Example: sudo docker exec -it <container_id> bash`
6. Inside container run these two command to create migration
`- Example: python3 manage.py makemigrations and python3 manage.py migrate`
7. Now create the superuser or you can directly signup new user.


## API Endpoints
1. Signup
`localhost:8000/api/signup/`
2. Login
`localhost:8000/api/login/`
3. Logout
`localhost:8000/api/logout/`
4. Create Task
`localhost:8000/api/task-create/`
5. List Task
`localhost:8000/api/tasks/`
6. Update Task
`localhost:8000/api/task-update/<int:pk>/`
7. Delete Task
`localhost:8000/api/task-delete/<int:pk>/`
8. Task Detail
`localhost:8000/api/task-detail/<int:pk>/`


## Interact in web
1. Signup
`http://localhost:8000/signup/`
2. Login
`http://localhost:8000/login/`
